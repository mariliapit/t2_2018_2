#ifndef _FAKEDISK_H_
#define _FAKEDISK_H_

#include <vector>

using namespace std;

#define I_NODE_SIZE 64

#define TIPO_BLOCO_LIVRE 0
#define TIPO_BLOCO_DIRETO 1
#define TIPO_BLOCO_INDIRETO 2

typedef struct{
	int size; // Número de blocos no sistema de arquivos
	int iSize; // Número de blocos de índice
	int freeList; // Primeiro bloco na lista de livres
} super_block;

typedef struct{
	int size = I_NODE_SIZE;
	bool available;
	int flags; // se flags = 0, ele não será usado
	int owner;
	int file_size; // tamanho em bytes
	int *pointer; // deve ser alocado na inicializacao
	bool opened;
	int blocosJaAlocados;
	int pAtual;
}i_node;

typedef struct {
	int type; //0 bloco_livre, 1 bloco_direto, 2 bloco_indireto
	int Indir1 = 0;
	int Indir2 = 0;
	int Indir3 = 0;
	int* pointersIndir1;
	int* pointersIndir2;
	int* pointersIndir3;
	int numeroBloco;
	char* content;
}data_block;

typedef struct {
	super_block* superbloco;
	i_node* blocosInode;
	data_block* blocosDeDados;
}disco_simulado;

typedef struct{
	int i_number;
	i_node* node;
}file_table_entry;


typedef struct{
	bool full;
	vector<int> i_numbers;
}file_table;


extern int NUM_BLOCKS;
extern int BLOCK_SIZE;
extern int POINTERS_PER_BLOCK;
extern int INODE_BLOCKS;
extern disco_simulado* disco;
extern file_table* tabelaDeArquivos;

/* Funcoes para mudar e obter o tamanho e numero de blocos simulados */
void set_block_size(int block_size); //default 512
int get_block_size();

void set_block_num(int block_num); //default 256
int get_block_num();

/* Como definido no livro */
void fd_read_raw(int block_num, char* buffer);

void fd_write_raw(int block_num, char* buffer);

void fd_read_super_block(int block_num, super_block* buffer);

void fd_write_super_block(int block_num, super_block* buffer);

void fd_read_i_node_block(int block_num, i_node* buffer);

void fd_write_i_node_block(int block_num, i_node* buffer);

void fd_read_indirect_block(int block_num, data_block* buffer);

void fd_write_indirect_block(int block_num, data_block* buffer);

void fd_stop();

// @mrj

super_block* criaSuperbloco(void);
i_node* criaBlocosInode(void);
data_block* criaBlocosLivres(void);
disco_simulado* criaDisco(void);
void criaTabelaArquivos(void);

int proxInodeLivre();
int proximoBlocoLivre();
void adicionaATabelaDeArquivos(int* i_number);
bool arquivoEstaAberto(int i_number);
void shutdown(void);

void INIT(void);

#endif /* _FAKEDISK_H_ */
