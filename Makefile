CC=g++

CFLAGS=-Wall -Wextra -Werror -O0 -g --std=c++11
LDFLAGS=-lm

.PHONY: all

all: exec

disk.o: disk.h disk.cpp
	$(CC) $(CFLAGS) -c disk.cpp

fs.o: fs.h fs.cpp
	$(CC) $(CFLAGS) -c fs.cpp

myTest: disk.o fs.o myTest.cpp
	$(CC) $(CFLAGS)  myTest.cpp -o myTest disk.o fs.o $(LDFLAGS)

aluno: myTest
	./myTest

test: disk.o fs.o test.cpp
	$(CC) $(CFLAGS)  test.cpp -o test disk.o fs.o $(LDFLAGS)

grade: test
	./test

clean:
	rm -rf *.o test myTest
