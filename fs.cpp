#include <fstream>
#include <cassert>
#include<iostream>
#include "disk.h"
#include "fs.h"

using namespace std;

extern int NUM_BLOCKS;
extern int BLOCK_SIZE;
extern int POINTERS_PER_BLOCK;
extern int INODE_BLOCKS;
extern int FREE_BLOCKS;
extern disco_simulado* disco;

int ffs_format_disk(int size, int i_size){
  // size = total de blocos
  // i_size = numero de blocos inode;
  NUM_BLOCKS = size;
  INODE_BLOCKS = i_size;

  if (i_size <= 0 || i_size >= size){
    return 0;
  }
  disco = criaDisco();

  return 1;
}


int ffs_create(){
  int i_number = proxInodeLivre();
 //when we create a file, we will have to allocate an inode for that file
 // must return inode number
 i_node* node = &(disco->blocosInode[i_number]);
 node->size = I_NODE_SIZE;
 node->file_size = 0;
 node->blocosJaAlocados = 0;
 node->available = false;

 if (node)
    return i_number;

return -1;
}

int ffs_open(int i_number){
  i_node* node = &(disco->blocosInode[i_number]);

  if(!node->available){
    adicionaATabelaDeArquivos(&i_number);
    node->pAtual = 0;
    return i_number;
  }
  return -1;
}

int ffs_write(int fd, char * buffer, int size){

  if(FREE_BLOCKS <= 0)
    return 0;

  if (arquivoEstaAberto(fd)){

    i_node* node = &(disco->blocosInode[fd]);

    if(node->blocosJaAlocados >= NUM_BLOCKS){
      return 0;
    }
      disco->superbloco->freeList =  proximoBlocoLivre();

      if (disco->superbloco->freeList == -1){
        return 0;
      }

      fd_write_raw(disco->superbloco->freeList, buffer);

      node->pointer[node->blocosJaAlocados] = disco->superbloco->freeList;

      if (node->blocosJaAlocados < 10){
        disco->blocosDeDados[disco->superbloco->freeList].type = TIPO_BLOCO_DIRETO;
        node->blocosJaAlocados++;
        node->file_size += size;
      }
      else if(node->blocosJaAlocados == 11){
        disco->blocosDeDados[11].type = TIPO_BLOCO_INDIRETO;

        if (!disco->blocosDeDados[11].Indir1){
          disco->blocosDeDados[11].pointersIndir1 = (int*) malloc(sizeof(int) * BLOCK_SIZE/4);
          disco->blocosDeDados[11].Indir1 = 0;
          FREE_BLOCKS--;
        }
        disco->blocosDeDados[11].pointersIndir1[disco->blocosDeDados[11].Indir1] = disco->superbloco->freeList;
        disco->blocosDeDados[disco->superbloco->freeList].type = TIPO_BLOCO_DIRETO;
        disco->blocosDeDados[11].Indir1++;
      }
      else if(node->blocosJaAlocados == 12){
        disco->blocosDeDados[12].type = TIPO_BLOCO_INDIRETO;

        if (!disco->blocosDeDados[12].Indir1){
          disco->blocosDeDados[12].pointersIndir2 = (int*) malloc(sizeof(int) * ((BLOCK_SIZE/4)^2));
          disco->blocosDeDados[12].Indir2 = 0;
          FREE_BLOCKS--;
        }
        disco->blocosDeDados[12].pointersIndir2[disco->blocosDeDados[12].Indir2] = disco->superbloco->freeList;
        disco->blocosDeDados[disco->superbloco->freeList].type = TIPO_BLOCO_DIRETO;
        disco->blocosDeDados[12].Indir2++;
      }
      else if(node->blocosJaAlocados == 13){
        disco->blocosDeDados[13].type = TIPO_BLOCO_INDIRETO;

        if (!disco->blocosDeDados[13].Indir3){
          disco->blocosDeDados[13].pointersIndir3 = (int*) malloc(sizeof(int) * ((BLOCK_SIZE/4)^2));
          disco->blocosDeDados[13].Indir3 = 0;
          FREE_BLOCKS--;
        }
        disco->blocosDeDados[13].pointersIndir2[disco->blocosDeDados[13].Indir3] = disco->superbloco->freeList;
        disco->blocosDeDados[disco->superbloco->freeList].type = TIPO_BLOCO_DIRETO;
        disco->blocosDeDados[13].Indir3++;
      }

      FREE_BLOCKS--;
      return 1;
    }
    else return 0;
}

int ffs_delete(int i_number){
  i_node* node = &(disco->blocosInode[i_number]);

  for(int i = node->blocosJaAlocados; i >= 0; i--){
    disco->blocosDeDados[node->pointer[i]].type = TIPO_BLOCO_LIVRE;
    for(int j = 0; j < BLOCK_SIZE; j++){
      disco->blocosDeDados[node->pointer[i]].content[j] = ' ';
      }
  }
  node->available = true;

  return 1;
}

int ffs_close(int fd){

  for (uint i=0; i < tabelaDeArquivos->i_numbers.size(); i++){
    if(tabelaDeArquivos->i_numbers.at(i) == fd){
     tabelaDeArquivos->i_numbers.erase(tabelaDeArquivos->i_numbers.begin()+i);
     i_node* node = &(disco->blocosInode[fd]);
     node->opened = false;
     return 1;
    }
  }
   return 0;
 }


int ffs_seek(int fd, int offset, int whence){
  i_node* node = &(disco->blocosInode[fd]);
  node->pAtual = offset + whence;
  return 1;
}

int ffs_read(int fd, char * buffer, int size){

  assert(size <= BLOCK_SIZE);
  i_node* node = &(disco->blocosInode[fd]);

  if(node->opened){
    if(!node->pointer[node->pAtual]){
      fd_read_raw(node->pointer[node->pAtual], buffer);
      node->pAtual++;
      return 1;
    }
  }

  return 0;
}
