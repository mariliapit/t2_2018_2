#include <cassert>
#include "disk.h"
#include "fs.h"
#include <iostream>

using namespace std;
//
using byte = uint8_t;
using uint = unsigned int;

int NUM_BLOCKS;
int FREE_BLOCKS;
int BLOCK_SIZE;
int POINTERS_PER_BLOCK;
int INODE_BLOCKS;
disco_simulado* disco;
file_table* tabelaDeArquivos;
int ESCRITAS = 0;
int LEITURAS = 0;

// @mrj

super_block* criaSuperbloco(void){
	super_block* superbloco = (super_block*) malloc(sizeof(super_block));

	superbloco->size = NUM_BLOCKS;
	superbloco->iSize = INODE_BLOCKS;
	superbloco->freeList = 0;

	return superbloco;
}

i_node* criaBlocosInode(void){
	int INODES_POR_BLOCO = BLOCK_SIZE/I_NODE_SIZE;

	i_node* blocosInode = (i_node*) malloc(sizeof(i_node) * INODE_BLOCKS * INODES_POR_BLOCO);

	for(int i = 0; i < INODE_BLOCKS*INODES_POR_BLOCO; i++){
		blocosInode[i].pointer = (int*) malloc(sizeof(int) * NUM_BLOCKS);
		blocosInode[i].available = true;
		// blocosInode[i].indirAloc1 = -1;
	}
	return blocosInode;
}

int proxInodeLivre(){

	int INODES_POR_BLOCO = BLOCK_SIZE/I_NODE_SIZE;

	for(int i = 0; i < INODE_BLOCKS*INODES_POR_BLOCO; i++){
		if (disco->blocosInode[i].available){
			disco->blocosInode[i].available = false;
			return i;
			}
	}
	return 0;
}

data_block* criaBlocosLivres(void){
	FREE_BLOCKS = NUM_BLOCKS - 1 - INODE_BLOCKS;
	data_block* blocosDeDados = (data_block*) malloc(sizeof(data_block) * FREE_BLOCKS);
  for(int i = 0; i < FREE_BLOCKS; i++){
			blocosDeDados[i].type = TIPO_BLOCO_LIVRE;
  }
	return blocosDeDados;
}

void criaTabelaArquivos(void){
		tabelaDeArquivos = (file_table*) malloc(sizeof(file_table));
		tabelaDeArquivos->full = false;
}

bool arquivoEstaAberto(int i_number){
	bool findInumber = false;
		for (uint i=0; i < tabelaDeArquivos->i_numbers.size(); i++){
			if(tabelaDeArquivos->i_numbers.at(i) == i_number)
			 findInumber = true;
		 }
		 return findInumber;
 }

void adicionaATabelaDeArquivos(int* i_number){
	i_node* node = &(disco->blocosInode[*i_number]);

  if(!arquivoEstaAberto(*i_number)){
		if(tabelaDeArquivos->i_numbers.size() < 20){
				tabelaDeArquivos->i_numbers.push_back(*i_number);
				node->opened = true;
				return;
			}
			else{
				tabelaDeArquivos->full = true;
				*i_number = -1;
				// cout << "tabela de arquivos cheia" << endl;
				return;
			}
		}
		// cout << "já está aberto" << endl;
}

disco_simulado* criaDisco(void){
  disco = (disco_simulado*) malloc(sizeof(disco_simulado));

  disco->superbloco = criaSuperbloco();
  disco->blocosInode = criaBlocosInode();
  disco->blocosDeDados = criaBlocosLivres();

	criaTabelaArquivos();
  return disco;
}

int proximoBlocoLivre(){
	uint BLOCOS_LIVRES = NUM_BLOCKS - 1 - INODE_BLOCKS;

	for(uint i = 0; i < BLOCOS_LIVRES; i++){
		if (disco->blocosDeDados[i].type ==  TIPO_BLOCO_LIVRE){
			return i;
			}
	}
	return -1;
}

void fd_write_raw(int block_num, char* buffer){
		data_block* bloco = &(disco->blocosDeDados[block_num]);

		if (bloco){
 			bloco->content = (char *) malloc(sizeof(char) * BLOCK_SIZE);
			}
		for(int i = 0; i < BLOCK_SIZE; i++){
				bloco->content[i] = buffer[i];
		}
		ESCRITAS++;
}

void fd_read_raw(int block_num, char* buffer){

	data_block* bloco = &(disco->blocosDeDados[block_num]);

	for(int i = 0; i < BLOCK_SIZE; i++){
		buffer[i] = bloco->content[i];
	}
	LEITURAS++;
}

// void fd_write_i_node_block(int block_num, i_node_block* buffer){
// 	//
// }

//tamanho e quantidade de blocos
void set_block_size(int block_size){
  BLOCK_SIZE = block_size;
	assert(ffs_format_disk(BLOCK_SIZE,1));
	// @mrj
}

int get_block_size(){
  return BLOCK_SIZE;
}

void set_block_num(int block_num){
  NUM_BLOCKS = block_num;
}

int get_block_num(){
  return NUM_BLOCKS;
}

//////

void fd_stop(void){
  cout << "\tDepuração e Estatísticas" << endl;
	cout << "Escritas de bloco realizadas: " << ESCRITAS << endl;
	cout << "Leituras de bloco realizadas: " << LEITURAS << endl;
}

void shutdown(void){
	for(int i = 0; i < 20; i++){
		if(arquivoEstaAberto(i)){
			ffs_close(i);
		}
	}
	fd_stop();
}

void INIT(void){
	return;
}
