#include "disk.h"
#include "simplegrade.h"
#include "fs.h"
#include <stdio.h>
#include<iostream>

using namespace std;

void teste_alocacao(){

	DESCRIBE("Teste de alocação");

	set_block_size(512);
	IF("Se tento alocar todos os blocos como inodes");
	THEN("Falho");
	isEqual(ffs_format_disk(512,512), 0, 1);

	IF("Se não tento alocar nenhum bloco como inode");
	THEN("Falho");
	isEqual(ffs_format_disk(512,0), 0, 1);

	IF("Se tento alocar um número razoável de blocos e inodes");
	THEN("Consigo");
	isEqual(ffs_format_disk(512,32), 1, 1);

}

void teste_leitura_e_escrita(){

	DESCRIBE("Teste de escrita/leitura");

	set_block_num(512);
	set_block_size(2048);
	ffs_format_disk(512,32);

	char buffer[BLOCK_SIZE];
	int i;
	for(i = 0; i < BLOCK_SIZE; i++){
		buffer[i] = 'X';
	}

	IF("Se tento escrever em um arquivo que não existe");
	THEN("Falho");
	isEqual(ffs_write(0, buffer, BLOCK_SIZE), 0, 1);

	int i_number = ffs_create();

	IF("Se tento escrever em um arquivo que existe, mas que não está na tabela de arquivos abertos");
	THEN("Falho");
	isEqual(ffs_write(i_number, buffer, BLOCK_SIZE), 0, 1);

	int fd = ffs_open(i_number);

	IF("Se tento escrever em um arquivo que existe e que está na tabela de arquivos abertos,");
	THEN("Consigo");
	isEqual(ffs_write(i_number, buffer, BLOCK_SIZE), 1, 1);

	ffs_close(fd);
	ffs_delete(i_number);

	int fds[21];
	int i_numbers[21];
	IF("Se tento abrir mais de 20 arquivos simultaneamente,");
	THEN("Falho");
	for(i = 0; i < 30; i++){
		i_numbers[i] = ffs_create();
		fds[i] = ffs_open(i_numbers[i]);
		if(fds[i] == -1)
			break;
	}
	isEqual(i, 20, 1);

	for(int i = 0; i < 20; i++){
		ffs_close(fds[i]);
		ffs_delete(i_numbers[i]);
	}
}

void teste_arquivo(){

	DESCRIBE("Teste de arquivo");

	set_block_num(32);
	set_block_size(512);
	ffs_format_disk(32,1);

	char buffer[BLOCK_SIZE];
	int i;
	for(i = 0; i < BLOCK_SIZE; i++){
		buffer[i] = 'X';
	}

	int i_number = ffs_create();
	int fd = ffs_open(i_number);

	IF("Se tento escrever em mais blocos que tenho,");
	THEN("Falho");
	for(i = 0; i < (NUM_BLOCKS*2); i++){
		if(!ffs_write(fd, buffer, BLOCK_SIZE)){
			i = -1;
			break;
		}
	}
	isEqual(i, -1, 1);

	ffs_close(fd);
	ffs_delete(i_number);

	i_number = ffs_create();
	fd = ffs_open(i_number);

	IF("Se tento escrever em um número razoável de blocos,");
	THEN("Consigo");
	for(i = 0; i < (NUM_BLOCKS-(NUM_BLOCKS/2)-(INODE_BLOCKS)); i++){
		ffs_write(fd, buffer, BLOCK_SIZE);
	}
	isEqual(i, (NUM_BLOCKS-(NUM_BLOCKS/2)-(INODE_BLOCKS)), 1);

	int i_number2 = ffs_create();
	int fd2 = ffs_open(i_number2);

	IF("Se ainda tenho blocos livres e tento escrever,");
	THEN("Consigo");
	for(i = 0; i < (NUM_BLOCKS-(NUM_BLOCKS/2)-(INODE_BLOCKS)); i++){
			ffs_write(fd2, buffer, BLOCK_SIZE);
	}
	isEqual(i, (NUM_BLOCKS-(NUM_BLOCKS/2)-(INODE_BLOCKS)), 1);

	ffs_close(fd);
	ffs_close(fd2);
	ffs_delete(i_number);
	ffs_delete(i_number2);

	shutdown();
}

int main(){

	teste_alocacao();
	teste_leitura_e_escrita();
	teste_arquivo();


	GRADEME();

	if (grade==maxgrade)
		return 0;
	else return grade;

}
